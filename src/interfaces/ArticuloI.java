package interfaces;

import java.util.ArrayList;

import model.ArticuloM;
import model.UsuarioM;

public interface ArticuloI {
	
	public String agregaArticulo(ArticuloM articulo);
	
	public ArrayList<ArticuloM> listaArticulos();

	public String eliminaArticulo(int id);
	
	public String modificarArticulo(ArticuloM articulo, int id);

}
