package interfaces;

import java.util.ArrayList;

import model.UsuarioM;

public interface UsuarioI {
	
	public String registraUsuario(UsuarioM usuario);
	
	public ArrayList<UsuarioM> listaUsuarios();
	
	public String validaUsuario(String nombre, String password);
	
	public String eliminaUsuario(int id);
	
	public String modificarUsuario(UsuarioM usuario, int id);

}
