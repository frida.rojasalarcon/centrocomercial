package model;

public class UsuarioM {

	private String nombre;
	private String apellido;
	private String correo;
	private String contrasena;
	private String telefono;
	private String nickname;
	private int id;
	
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "UsuarioM [nombre=" + nombre + ", apellido=" + apellido + ", correo=" + correo + ", contrasena="
				+ contrasena + ", telefono=" + telefono + ", nickname=" + nickname + "]";
	}
	public UsuarioM() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UsuarioM(String nombre, String apellido, String correo, String contrasena, String telefono, String nickname) {
		super();

		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.contrasena = contrasena;
		this.telefono = telefono;
		this.nickname = nickname;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
}
