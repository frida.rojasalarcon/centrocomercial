package model;

import java.time.LocalDateTime;

public class ArticuloM {
	
	private int id;
	private String nombre;
	private String marca;
	private String imagen;
	private double precio;
	private int stock;
	private int categoria;
	private LocalDateTime fecha;
	
	
	
	
	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	@Override
	public String toString() {
		return "id=" + id + ", nombre=" + nombre + ", marca=" + marca + ", precio=" + precio + ", stock="
				+ stock + ", categoria=" + categoria + "]";
	}
	
	public ArticuloM() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ArticuloM(String nombre, String marca, double precio, int stock, int categoria, LocalDateTime fecha) {
		super();
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
		this.stock = stock;
		this.categoria = categoria;
		this.fecha=fecha;
	}
	public ArticuloM(String nombre, String marca, double precio, int stock, int categoria) {
		super();
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
		this.stock = stock;
		this.categoria = categoria;
	}
	
	
	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getCategoria() {
		return categoria;
	}
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	

}
