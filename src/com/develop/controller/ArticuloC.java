package com.develop.controller;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ArticuloDAO;
import com.develop.DAO.UsuarioDAO;

import model.ArticuloM;
import model.UsuarioM;

/**
 * Servlet implementation class ArticuloC
 */
@WebServlet("/ArticuloC")
public class ArticuloC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ArticuloC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());

		String respuesta = "";
		ArticuloDAO dao = new ArticuloDAO();

		//////////////////////////////// AGREGA ARTICULO//////////////////////////////////////
		try {
			if (request.getParameter("agregar") != null) {

				String nombre;
				String marca;
				double precio;
				int stock;
				int categoria;

				nombre = request.getParameter("nombre");
				marca = request.getParameter("marca");
				precio = Double.parseDouble(request.getParameter("precio"));
				stock = Integer.parseInt(request.getParameter("stock"));
				categoria = Integer.parseInt(request.getParameter("categoria"));

				ArticuloM per = new ArticuloM(nombre, marca, precio, stock, categoria);

				respuesta = dao.agregaArticulo(per);
				
				if (respuesta.equals("OK")) {
					RequestDispatcher rq;
					rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
					rq.forward(request, response);

				} else {
					System.out.println("no valido");
				}
				
				
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		//////////////////////////////////////////// ELIMINAR ARTICULO////////////////////////

		try {
			if (request.getParameter("borrar") != null) {
				int id;

				id = Integer.parseInt(request.getParameter("id"));

				respuesta = dao.eliminaArticulo(id);

				System.out.println(respuesta);
				if (respuesta.equals("OK")) {
					RequestDispatcher rq;
					rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
					rq.forward(request, response);

				} else {
					System.out.println("no valido");
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		/////////////////////////////////////////// ACTUALIZAR ARTICULO//////////////////////////

		try {
			if (request.getParameter("modificar") != null) {
				String nombre;
				String marca;
				double precio;
				int stock;
				int categoria;
				int id;

				// obtinene campos de DatosUsuario
				id = Integer.parseInt(request.getParameter("id"));
				nombre = request.getParameter("nombre");
				marca = request.getParameter("marca");
				precio = Double.parseDouble(request.getParameter("precio"));
				stock = Integer.parseInt(request.getParameter("stock"));
				categoria = Integer.parseInt(request.getParameter("categoria"));
				

				ArticuloM per = new ArticuloM(nombre, marca, precio, stock, categoria);

				respuesta = dao.modificarArticulo(per, id);

				System.out.println(respuesta);
				if (respuesta.equals("OK")) {
					RequestDispatcher rq;
					rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
					rq.forward(request, response);

				} else {
					System.out.println("no valido");
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		//////////////////////////////////////// EXPORTACIONES//////////////////////////////////
		
		
		//////////txt///////////
		try {
			if (request.getParameter("texto") != null) {
				
				respuesta=dao.muestraEnArchivo();
				if (respuesta.equals("OK")) {
					RequestDispatcher rq;
					rq = request.getRequestDispatcher("/vistas/Administrador/Articulos/VistaArticulos.jsp");
					rq.forward(request, response);

				} else {
					System.out.println("no valido");
				}
			}
		}catch(Exception e) {
			
		}
		
		/////////////pdf/////////////
		try {
			if (request.getParameter("pdf") != null) {
				
				respuesta=dao.muestraEnPdf();
				if (respuesta.equals("OK")) {
					RequestDispatcher rq;
					rq = request.getRequestDispatcher("/vistas/Administrador/Articulos/VistaArticulos.jsp");
					rq.forward(request, response);

				} else {
					System.out.println("no valido");
				}
			}
		}catch(Exception e) {
			
		}
		
///////////// EXCEL /////////////
	try {
		if (request.getParameter("excel") != null) {
			
			respuesta=dao.muestraEnExcel();
			if (respuesta.equals("OK")) {
				RequestDispatcher rq;
				rq = request.getRequestDispatcher("/vistas/Administrador/Articulos/VistaArticulos.jsp");
				rq.forward(request, response);

			} else {
				System.out.println("no valido");
			}
		}
	}catch(Exception e) {
		
	}
		
		
		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
