package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.UsuarioDAO;

import model.UsuarioM;

/**
 * Servlet implementation class UsuarioC
 */
@WebServlet("/UsuarioC")
public class UsuarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsuarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		//elchido
		
		String respuesta="";
		UsuarioDAO dao=new UsuarioDAO();
		
		//////////////////////////////// AGREGA USUARIO//////////////////////////////////////
		if(request.getParameter("agregar")!=null) {
			String nombre;
			String apellido;
			String correo;
			String contrasena;
			String telefono;
			String nickname;
			
			try {
			//obtinene campos de DatosUsuario
			nombre=request.getParameter("nombre");
			apellido=request.getParameter("apellido");
			nickname=request.getParameter("nickname");
			correo=request.getParameter("correo");
			contrasena=request.getParameter("contrasena");
			telefono=request.getParameter("telefono");
			
			
			UsuarioM  per=new UsuarioM(nombre, apellido, correo, contrasena, telefono, nickname);
			
			respuesta=dao.registraUsuario(per);
			
			System.out.println(respuesta);
			if(respuesta.equals("OK")) {
				RequestDispatcher rq;
				rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
				rq.forward(request, response);
				
			}else {
				System.out.println("no valido");
			}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
		}
		
		////////////////////////////////////////// VALIDA USUARIO, LOGIN /////////////////////////////
		if(request.getParameter("login")!=null) {
			
			String usuario;
			String password;
			
			try {
			usuario=request.getParameter("nombre");
			password=request.getParameter("contrasena");
			
			
			respuesta=dao.validaUsuario(usuario, password);
			
			System.out.println(respuesta);
			
			if(respuesta.equals("admin")) {
				System.out.println("redireccionando");
				RequestDispatcher rq;
				//rq = request.getRequestDispatcher("http://localhost:8080/CentroComercial/vistas/Administrador/PantallaAdmin.jsp");
				rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
				rq.forward(request, response);
				
			}if(respuesta.equals("cliente")) {
					RequestDispatcher rq;
					//rq = request.getRequestDispatcher("http://localhost:8080/CentroComercial/vistas/Cliente/Inicio.jsp");
					rq = request.getRequestDispatcher("/vistas/Cliente/Inicio.jsp");
					rq.forward(request, response);
					
				}else {
					System.out.println("no encontrado");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			
		}
		
		
		////////////////////////////////////////// ELIMINA USUARIO //////////////////////////////////
		
		if(request.getParameter("borrar")!=null) {
			int id;
			
			try {
			
			id=Integer.parseInt(request.getParameter("id"));
			
			respuesta= dao.eliminaUsuario(id);
			
			System.out.println(respuesta);
			if(respuesta.equals("OK")) {
				RequestDispatcher rq;
				rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
				rq.forward(request, response);
				
			}else {
				System.out.println("no valido");
			}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			
			
		}
		
		
		/////////////////////////////////////////// MODIFICA USUARIO///////////////////////////
		
		
		if(request.getParameter("modificar")!=null) {
			String nombre;
			String apellido;
			String correo;
			String contrasena;
			String telefono;
			String nickname;
			int id; 
			
			try {
			//obtinene campos de DatosUsuario
			id=Integer.parseInt(request.getParameter("id"));
			nombre=request.getParameter("nombre");
			apellido=request.getParameter("apellido");
			nickname=request.getParameter("nickname");
			correo=request.getParameter("correo");
			contrasena=request.getParameter("contrasena");
			telefono=request.getParameter("telefono");
			
			
			UsuarioM  per=new UsuarioM(nombre, apellido, correo, contrasena, telefono, nickname);
			
			respuesta=dao.modificarUsuario(per,id);
			
			System.out.println(respuesta);
			if(respuesta.equals("OK")) {
				RequestDispatcher rq;
				rq = request.getRequestDispatcher("/vistas/Administrador/PantallaAdmin.jsp");
				rq.forward(request, response);
				
			}else {
				System.out.println("no valido");
			}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
		}
		
		
		
		dao.listaUsuarios();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
