package com.develop.DAO;

import interfaces.ArticuloI;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import model.ArticuloM;
import model.UsuarioM;

import com.develop.config.ConexionDB;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.sql.PreparedStatement;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class ArticuloDAO implements ArticuloI {

	ConexionDB conexion = new ConexionDB();
	Statement st;
	Connection consql;
	PreparedStatement ps;
	ResultSet rs;

	@Override
	public String agregaArticulo(ArticuloM articulo) {

		String sql = "INSERT INTO `articulos` (`nombre`, `marca`, `precio`, `stock`, `id_categoria`) VALUES ('"
				+ articulo.getNombre() + "', '" + articulo.getMarca() + "', '" + articulo.getPrecio() + "', '"
				+ articulo.getStock() + "', '" + articulo.getCategoria() + "')";
		String retorno = "";
		System.out.println(sql);

		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			ps.executeUpdate();
			retorno = "OK";

		} catch (Exception e) {
			System.out.println(e);
		}
		// TODO Auto-generated method stub
		return retorno;
	}

	@Override
	public ArrayList<ArticuloM> listaArticulos() {
		ArrayList<ArticuloM> lista = new ArrayList<>();
		PedidoDAO pedido = new PedidoDAO();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		try {
			consql = conexion.getConnection();
			st = consql.createStatement();
			rs = st.executeQuery("select * from articulos");
			while (rs.next()) {
				ArticuloM articulo = new ArticuloM();
				articulo.setId(Integer.parseInt(rs.getString("id_articulo")));
				articulo.setNombre(rs.getString("nombre"));
				articulo.setMarca(rs.getString("marca"));
				articulo.setPrecio(Double.parseDouble(rs.getString("precio")));
				articulo.setStock(Integer.parseInt(rs.getString("stock")));
				articulo.setCategoria(Integer.parseInt(rs.getString("id_categoria")));

				articulo.setFecha(LocalDateTime.parse(rs.getString("fechaHora").substring(0, 19), formatter));

				articulo.setImagen(rs.getString("imagen"));
				lista.add(articulo);

			}

		} catch (Exception e) {
			System.out.println(e);
		}

		return lista;

	}

	@Override
	public String eliminaArticulo(int id) {
		String sql = "DELETE FROM `articulos` WHERE `articulos`.`id_articulo` = " + id + "";
		String retorno = "";

		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);
			ps.executeUpdate();
			retorno = "OK";

		} catch (Exception e) {
			System.out.println(e);
		}

		return retorno;
	}

	@Override
	public String modificarArticulo(ArticuloM articulo, int id) {

		String sql = "UPDATE articulos SET nombre=?, marca=?, precio=?, stock=?, id_categoria=? WHERE id_articulo="
				+ id;
		String retorno = "";

		System.out.println(sql);
		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps = consql.prepareStatement(sql);

			ps.setString(1, articulo.getNombre());
			ps.setString(2, articulo.getMarca());
			ps.setString(3, Double.toString(articulo.getPrecio()));
			ps.setString(4, Integer.toString(articulo.getStock()));
			ps.setString(5, Integer.toString(articulo.getCategoria()));
			ps.executeUpdate();

			retorno = "OK";

		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println(retorno);

		return retorno;

	}

	public String muestraEnArchivo() {

		String retorno = "";
		ArrayList<ArticuloM> lista = new ArrayList<>();

		System.out.println("entre a mostrar archivo");
		File archivo = new File(
				"C:\\Users\\adirF\\Documents\\GitLab\\centrocomercial\\src\\com\\develop\\DAO/Articulos3.txt");
		lista = listaArticulos();

		try {
			boolean band = archivo.createNewFile();
			FileWriter fileWriter = new FileWriter(archivo);

			for (ArticuloM a : lista) {
				fileWriter.write("\nNombre: " + a.getNombre() + "   Categoria: " + a.getCategoria());
				System.out.println(a.getNombre());

			}

			fileWriter.close();

			Scanner esc = new Scanner(archivo);

			if (band) {
				System.out.println("se creo el archivo");

				while (esc.hasNext()) {
					System.out.println(esc.nextLine());

				}
				retorno = "OK";

			} else {
				System.out.println("no se creo el archivo");
				while (esc.hasNext()) {
					System.out.println(esc.nextLine());
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return retorno;
	}

	public String muestraEnPdf() {

		String retorno = "";
		ArrayList<ArticuloM> lista = new ArrayList<>();

		try {
			// Se crea el documento
			Document documento = new Document();

			// Se crea el OutputStream para el fichero donde queremos dejar el pdf.
			FileOutputStream ficheroPdf = new FileOutputStream(
					"C:\\Users\\adirF\\Documents\\GitLab\\centrocomercial\\src\\com\\develop\\DAO/Articulos.pdf");
			lista = listaArticulos();

			// Se asocia el documento al OutputStream y se indica que el espaciado entre
			// lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
			PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);

			// Se abre el documento.
			documento.open();

			for (ArticuloM a : lista) {
				documento.add(new Paragraph("\nNombre: " + a.getNombre() + "   Categoria: " + a.getCategoria()));
				System.out.println(a.getNombre());

			}

			documento.close();
			retorno = "OK";

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return retorno;
	}

	public String muestraEnExcel() {

		String retorno = "";
		ArrayList<ArticuloM> lista = new ArrayList<>();
		lista = listaArticulos();
		File xlsFile = new File("C:\\Users\\adirF\\Documents\\GitLab\\centrocomercial\\src\\com\\develop\\DAO/Articulos.xls");

		try {
			WritableWorkbook libro = Workbook.createWorkbook(xlsFile);
			WritableSheet hojita = libro.createSheet ("Libreta de direcciones", 0);
			Label labelC = new Label(0, 0, "This is a Label cell");

			int col;
			for (int row = 0; row <= 3; row++){
				col=0;
				for(ArticuloM a : lista) {
					
					switch(row) {
		        	 case 0:
		        		 hojita.addCell(new Label(row, col, "Nombre"));
		        		 break;
		        	 case 1:
		        		 hojita.addCell(new Label(row, col, a.getNombre()));
		        		 break;
		        	 case 2:
		        		 hojita.addCell(new Label(row, col, "Categoria"));
		        		 break;
		        	 case 3:
		        		 hojita.addCell(new Label(row, col, Integer.toString(a.getCategoria()) ));
		        		 break;
		        	 }
					
					col++;
				}
			}
		
			
			libro.write();
		    libro.close();
			
			retorno = "OK";

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return retorno;
	}

}
