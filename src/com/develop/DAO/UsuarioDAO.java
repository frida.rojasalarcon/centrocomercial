package com.develop.DAO;

import com.develop.config.ConexionDB;
import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.PreparedStatement;
import interfaces.UsuarioI;
import model.ArticuloM;
import model.UsuarioM;

public class UsuarioDAO implements UsuarioI{
	
	ConexionDB conexion=new ConexionDB();
    Connection consql;
    Statement st;
    PreparedStatement ps;
    ResultSet rs;
	UsuarioM usuario = new UsuarioM();

	@Override
	public String registraUsuario(UsuarioM usuario) {
		String sql="INSERT INTO `usuario` (`nombre`, `apellidos`, `correo`, `contrasena`, `id_tipoUsuario`, `telefono`, `nickname`) VALUES ('"+usuario.getNombre()+"', '"+usuario.getApellido()+"', '"+usuario.getCorreo()+"', '"+usuario.getContrasena()+"', '1', '"+usuario.getTelefono()+"', '"+usuario.getNickname()+"')";
		String retorno="";
		System.out.println(sql);
		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps= consql.prepareStatement(sql);
			ps.executeUpdate();
			retorno="OK";
			
		}catch(Exception e){
			System.out.println(e);
		}
		
		
		return retorno;
	}

	@Override
	public ArrayList<UsuarioM> listaUsuarios() {
		ArrayList<UsuarioM>lista=new ArrayList<>();
		
		
		
		try{
		            consql = conexion.getConnection();
		            st= consql.createStatement();
					rs = st.executeQuery("select * from usuario");
		            while(rs.next()){
		            	UsuarioM usuario= new UsuarioM();
		            	usuario.setId(Integer.parseInt(rs.getString("id_usuario")));
		            	usuario.setNombre(rs.getString("nombre"));
		            	usuario.setApellido(rs.getString("apellidos"));
		            	usuario.setContrasena(rs.getString("contrasena"));
		            	usuario.setTelefono(rs.getString("telefono"));
		            	usuario.setNickname(rs.getString("nickname"));
		            	usuario.setCorreo(rs.getString("correo"));
		                lista.add(usuario);
		                
		            }
		            
		        }catch(Exception e){
		            System.out.println(e);
		        }
		        
		       
		return lista;
				
		
	}
	
	@Override
	public String validaUsuario(String nombre, String password) {
		
		String sql="SELECT * FROM `usuario` WHERE nombre='"+nombre+"' AND contrasena='"+password+"'";
		String retorno ="";
		
		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps= consql.prepareStatement(sql);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				if(Integer.parseInt(rs.getString("id_tipoUsuario"))==1) {
					retorno = "cliente";
				}else {
					if(Integer.parseInt(rs.getString("id_tipoUsuario"))==2) {
						retorno = "admin";
					}
				}
				
			}
			
			System.out.println("DAO: "+retorno);
			
		}catch(Exception e){
			System.out.println(e);
		}
	
	return retorno;
	}
	
	@Override
	public String eliminaUsuario(int id) {
		String sql="DELETE FROM `usuario` WHERE `usuario`.`id_usuario` = "+id+"";
		String retorno ="";
		
		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps= consql.prepareStatement(sql);
			ps.executeUpdate();
			retorno="OK";
			
		}catch(Exception e){
			System.out.println(e);
		}
		
		return retorno;
	}

	@Override
	public String modificarUsuario(UsuarioM usuario, int id) {
		
		String sql="UPDATE usuario SET nombre=?, apellidos=?, correo=?, contrasena=?, id_tipoUsuario=?, telefono=?, nickname=?  WHERE id_Usuario="+id;
		String retorno="";
		String tipo_usuario= "1";
		System.out.println(sql);
		try {
			System.out.println(sql);
			consql = conexion.getConnection();
			ps= consql.prepareStatement(sql);
			ps.setString(1, usuario.getNombre());
			ps.setString(2, usuario.getApellido());
			ps.setString(3, usuario.getCorreo());
			ps.setString(4, usuario.getContrasena());
			ps.setString(5, tipo_usuario);
			ps.setString(6, usuario.getTelefono());
			ps.setString(7, usuario.getNickname());
			ps.executeUpdate();
			
			retorno="OK";
			
		}catch(Exception e){
			System.out.println(e);
		}
		System.out.println(retorno);
		
		return retorno;

	}
}
