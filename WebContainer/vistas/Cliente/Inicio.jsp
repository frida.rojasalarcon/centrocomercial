<%-- 
    Document   : Inicio
    Created on : 8/10/2021, 10:51:52 AM
    Author     : adirF
--%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="model.ArticuloM"%>
<%@include file="HeaderCliente.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="/CentroComercial/vistas/Cliente/estiloCardsInicio.css">
    </head>
    <body>
        <!------------ CARRUSEL---->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="https://www.mistore.mx/modules/posslideshows/images/1c47cc4b67dceb273902362c7a68cdccad3c9967_MiA3_banner_tiny.jpg" alt="">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://thefoodtech.com/wp-content/uploads/2021/01/Frutas-y-verduras.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src=".../800x400?auto=yes&bg=555&fg=333&text=Third slide" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!---------- TARJETAS -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

<% ArticuloDAO dao = new ArticuloDAO();%>
<%for (ArticuloM articulo : dao.listaArticulos()) {%>


<div class="container bootstrap snippets bootdeys ">
    <div class="col-md-4">
       <div class="card card-background">
            <div class="image" style="background-image: url(<%= articulo.getImagen()%>); background-size: cover; background-position: 50% 50%;">
                <div class="filter"></div>
            </div>
             <div class="content">
                <h5 class="price"><%= articulo.getNombre() %>
                     <a href=<%="http://localhost:8080/CentroComercial/vistas/Cliente/VerArticulo.jsp?id="+articulo.getId()%> class="pull-right">
                        Quiero verlo!
                     </a>
                 </h5> 
            </div>
            <div class="footer">
               <div class="author">
                    <a href="#">
                       <span><%String caso="";
								switch(articulo.getCategoria()){
									case 1: caso="Tecnologia";break;
									case 2: caso="Hogar";break;
									case 3: caso="Farmacia";break;
									case 4: caso="Super";break;
							    }%><%=caso %></span>
                    </a>
                </div>  
                <div class="stats pull-right">
                     $<%= articulo.getPrecio()%>                                
               </div>                        
            </div>
        </div>
    </div>
</div>

<%} %>
    </body>
    <footer>
       
    </footer>
</html>
