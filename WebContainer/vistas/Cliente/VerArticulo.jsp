<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="model.ArticuloM"%>    

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/CentroComercial/vistas/Cliente/estiloVerArticulo.css">
</head>
<body>

	                        

                        
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<div class="v-card">
	<!-- Header -->
	<% ArticuloDAO dao = new ArticuloDAO();
			                  %>

								<%
								int id=Integer.parseInt(request.getParameter("id"));
								for (ArticuloM articulo : dao.listaArticulos()) {
									
					                   if(articulo.getId()==id ){
			                   %>
	
	<header>
		<button  id="modificar" name="modificar" type="submit"
										class="btn btn-primary">Agregar al carrito</button>
	</header>
	<!-- tab Content -->
	
			<!-- Home Image -->
			<div class="vcard-img">
				<img src="<%= articulo.getImagen()%>"  alt="" class="img-responsive img-thumbnail">
			</div>
			<div class="vcard-content">
				<!-- Name -->
				<h4><%=articulo.getNombre() %> </h4>
			</div>
			
		
		<!-- Tab Pane - Education & Experience -->
		<div class="tab-pane fade" id="qualification">
			<div class="education">
				<h4><i class="fa fa-database color"></i> <%
										String caso="";
										switch(articulo.getCategoria()){
											case 1: caso="Tecnologia";break;
											case 2: caso="Hogar";break;
											case 3: caso="Farmacia";break;
											case 4: caso="Super";break;
										}
										%><%=caso %></span></td></h4>
				
				<div class="row">
					<div class="col-md-6 col-sm-6">
							<!-- Eudcation detail and year -->
							<h5><%=articulo.getMarca() %></h5>
					</div>
					<div class="col-md-6 col-sm-6">
							<h5>$<%=articulo.getPrecio() %> / <small><%=articulo.getStock() %> Disponibles</small></h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>   
<%
                       
                       
                       
					                   }
                          }
                  %>                                     

</body>
</html>