

<%@page import="com.develop.DAO.UsuarioDAO"%>
<%@page import="model.UsuarioM"%>
<%@page import="java.util.ArrayList"%>

<%@include file="../HeaderAdmin.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../estiloVista.css">


<nav style="background-color: transparent;">
	<form class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="search" placeholder="Search"
			aria-label="Search">
		<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>



	</form>

	<span class="navbar-text">
		<form action="AgregaCliente.jsp">
			<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Agregar
				Cliente</button>
		</form>
	</span>

</nav>

</head>
<body>



	<link
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
		rel="stylesheet">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-box clearfix">
					<div class="table-responsive">
						<table class="table user-list">
							<thead>
								<tr>
									<th><span>ID</span></th>
									<th><span>Usuario</span></th>
									<th><span>Nickname</span></th>
									<th class="text-center"><span>Telefono</span></th>
									<th><span>Email</span></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<% UsuarioDAO dao = new UsuarioDAO();
			                  %>

								<%
			                  for (UsuarioM usuario : dao.listaUsuarios()) {
			                   %>
								<tr>
									<td><%= usuario.getId() %></td>
									<td><img
										src="https://bootdey.com/img/Content/avatar/avatar1.png"
										alt=""> <a ><%= usuario.getNombre() %></a>
										<span><%= usuario.getApellido() %></span>
										<span class="user-subhead">Cliente</span></td>

									<td><%= usuario.getNickname() %></td>

									<td class="text-center"><span class="label label-default"><%= usuario.getTelefono() %></span>
									</td>

									<td><a><%= usuario.getCorreo() %></a></td>

									<td style="width: 20%;">
										

										<form method="POST" action=<%="http://localhost:8080/CentroComercial/UsuarioC?id="+usuario.getId()%>>
											<button type="submit" name="borrar" onclick="http://localhost:8080/CentroComercial/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>


										<form method="POST" action=<%="/CentroComercial/vistas/Administrador/Clientes/EditaCliente.jsp?id="+usuario.getId()%>>
											<button type="submit" name="modificar" onclick="http://localhost:8080/CentroComercial/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>

									</td>
								</tr>
								<%
                       
                       
                       
 
                          }
                  %>


							</tbody>
						</table>
					</div>
					<ul class="pagination pull-right">
						<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>