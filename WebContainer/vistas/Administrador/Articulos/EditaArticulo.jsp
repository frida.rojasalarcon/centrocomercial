
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="model.ArticuloM"%>
<%@page import="java.util.ArrayList"%>

<%@include file="../HeaderAdmin.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../estiloAgrega.css">
</head>
<body>

	<div class="container">
		<div class="row gutters">
			<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
				<div class="card h-100">
					<div class="card-body">
						<div class="account-settings">
							<div class="user-profile">
								<div class="user-avatar">
									<img
										src="https://media.istockphoto.com/photos/portrait-of-smiling-handsome-man-in-blue-tshirt-standing-with-crossed-picture-id1045886560?s=612x612"
										alt="Maxwell Admin">
								</div>
								<h5 class="user-name">¡Hola!</h5>
								<h6 class="user-email">Estas editando un articulo</h6>
							</div>
							<div class="about">
								<h5 class="mb-2 text-primary">About</h5>
								<p>Gracias por tu excelente trabajo!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
				<div class="card h-100">
				
				
				<% ArticuloDAO dao = new ArticuloDAO();
			                  %>

								<%
								int id=Integer.parseInt(request.getParameter("id"));
								for (ArticuloM articulo : dao.listaArticulos()) {
									
					                   if(articulo.getId()==id ){
			                   %>
				
				<form method="post" action=<%="http://localhost:8080/CentroComercial/ArticuloC?id="+id%> name="modificar">
					<div class="card-body">
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<h6 class="mb-3 text-primary">Toma de datos</h6>
							</div>
							
							
							
							

								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Nombre del articulo</label> <input type="text"
											class="form-control" id="nombre" name="nombre"
											value=<%=articulo.getNombre() %>>
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Marca</label> <input type="text"
											class="form-control" id="marca" name="marca"
											value=<%=articulo.getMarca() %>>
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Precio</label> <input type="text"
											class="form-control" id="precio" name="precio"
											value=<%=articulo.getPrecio() %>>
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Stock</label> <input type="text"
											class="form-control" id="stock" name="stock"
											value=<%=articulo.getStock() %>>
									</div>
								</div>
						</div>
						<div class="row gutters">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
								<div class="form-group">
									<label>Categoria</label> <input type="text" class="form-control"
										id="categoria" name="categoria" value=<%=articulo.getCategoria() %>>
								</div>
							</div>
							
							<div>
								<form>
									<div class="form-group">
										<label for="exampleFormControlFile1">Agregue foto de
											perfil </label> <input type="file" class="form-control-file"
											id="imagen" name= "imagen">
									</div>
								</form>
							</div>


						</div>
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="text-right">
									<button  id="modificar" name="modificar" type="submit"
										class="btn btn-primary">Modificar</button>
								</div>
							</div>
						</div>

					</div>
					</form>
					<%
                       
                       
                       
					                   }
                          }
                  %>
				</div>
			</div>
		</div>
	</div>
</body>
</html>