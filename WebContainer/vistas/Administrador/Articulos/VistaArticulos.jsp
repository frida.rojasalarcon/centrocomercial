
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.develop.DAO.ArticuloDAO"%>
<%@page import="model.ArticuloM"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.time.LocalDateTime"%>

<%@include file="../HeaderAdmin.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../estiloVista.css">


<nav style="background-color: transparent;">
	<form class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="search" placeholder="Search"
			aria-label="Search">
		<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>



	</form>

	<span class="navbar-text">
		<form action="AgregaArticulo.jsp">
			<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Agregar
				Articulo</button>
		</form>
	</span> 
	
	<span class="navbar-text">
		<form action="http://localhost:8080/CentroComercial/ArticuloC">
			<button type="submit" class="btn btn-outline-dark my-2 my-sm-0" id="texto" name="texto">
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
					fill="currentColor" class="bi bi-file-text-fill"
					viewBox="0 0 16 16">
  <path
						d="M12 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM5 4h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1zm-.5 2.5A.5.5 0 0 1 5 6h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zM5 8h6a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1zm0 2h3a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1z" />
</svg>
				Exportar a txt
			</button>
		</form>
		
		
	</span> <span class="navbar-text">
		<form action="http://localhost:8080/CentroComercial/ArticuloC">
			<button type="submit" class="btn btn-outline-dark my-2 my-sm-0" id="excel" name="excel">
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
					fill="currentColor" class="bi bi-file-earmark-excel-fill"
					viewBox="0 0 16 16">
  <path
						d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM5.884 6.68 8 9.219l2.116-2.54a.5.5 0 1 1 .768.641L8.651 10l2.233 2.68a.5.5 0 0 1-.768.64L8 10.781l-2.116 2.54a.5.5 0 0 1-.768-.641L7.349 10 5.116 7.32a.5.5 0 1 1 .768-.64z" />
</svg>
				Exportar a excel
			</button>
		</form>
	</span> <span class="navbar-text">
		<form action="http://localhost:8080/CentroComercial/ArticuloC">
			<button type="submit" class="btn btn-outline-dark my-2 my-sm-0" id="pdf" name="pdf">
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
					fill="currentColor" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
  <path
						d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z" />
  <path fill-rule="evenodd"
						d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z" />
</svg>
				Exportar a pdf
			</button>
		</form>
	</span>


</nav>

</head>
<body>



	<link
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
		rel="stylesheet">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-box clearfix">
					<div class="table-responsive">
						<table class="table user-list">
							<thead>
								<tr>
									<th><span>ID</span></th>
									<th><span>Articulo</span></th>
									<th><span>Marca</span></th>
									<th class="text-center"><span>precio</span></th>
									<th><span>Fecha Registro</span></th>
									<th><span>Promocion</span></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<% ArticuloDAO dao = new ArticuloDAO();
			                  %>

								<%
			                  for (ArticuloM articulo : dao.listaArticulos()) {
			                   %>
								<tr>
									<td><%= articulo.getId() %></td>
									<td><img src="<%= articulo.getImagen()%>"
										<%System.out.println(articulo.getImagen()); %> alt=""> <a
										href="#" class="user-link"><%= articulo.getNombre() %></a> <span
										class="user-subhead">
											<%
										String caso="";
										switch(articulo.getCategoria()){
											case 1: caso="Tecnologia";break;
											case 2: caso="Hogar";break;
											case 3: caso="Farmacia";break;
											case 4: caso="Super";break;
										}
										%><%=caso %></span></td>

									<td><%= articulo.getMarca() %></td>

									<td class="text-center"><span class="label label-default"><%= articulo.getPrecio()%></span>
									</td>

									<td><a> <%= 
									
										"Fecha:  "+articulo.getFecha().getDayOfMonth()+"/"+articulo.getFecha().getMonthValue()+"/"+articulo.getFecha().getYear()+" \n Hora:   "+articulo.getFecha().getHour()+":"+articulo.getFecha().getMinute()+":"+articulo.getFecha().getSecond()
									 %></a></td>

									<td><a>PENDIENTE</a></td>

									<td style="width: 20%;">


										<form method="POST"
											action=<%="http://localhost:8080/CentroComercial/ArticuloC?id="+articulo.getId()%>>
											<button type="submit" name="borrar"
												onclick="http://localhost:8080/CentroComercial/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>


										<form method="POST"
											action=<%="/CentroComercial/vistas/Administrador/Articulos/EditaArticulo.jsp?id="+articulo.getId()%>>
											<button type="submit" name="modificar"
												onclick="http://localhost:8080/CentroComercial/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>

									</td>
								</tr>
								<%
                       
                       
                       
 
                          }
                  %>


							</tbody>
						</table>
					</div>
					<ul class="pagination pull-right">
						<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>