<%-- 
    Document   : Header
    Created on : 7/10/2021, 10:35:43 AM
    Author     : adirF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="styles.css">

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="Icon">
                <!--Icono de usuario-->
                <span class="glyphicon-user"></span>
            </div>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="Inicio.jsp">Administrador <span class="sr-only">(current)</span></a>
                    </li>
                    
                    
                </ul>
                
                <span class="navbar-text">
                    <form action="">
                        <button class="btn btn-primary" type="submit"  >Salir</button>
                    </form>
                </span>

            </div>
        </nav>
    </body>
</html>
